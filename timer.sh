#!/usr/bin/env bash
#
# timer.sh - É um timer em shell script
# Autor: Jesher Minelli Alves, https://github.com/jesherdevsk8/Shell-Script/blob/master/programas/timer.sh
# Execução: $ ./timer.sh
# Data: 01/12/2021 - update: 09/07/2024
# 
# -- Cores
CYAN="\033[36;1m"
RED="\033[31;1m"
GREEN="\033[32;1m"
RESET="\033[0m"

# -- Funções

timer() {
  local total_seconds=$1

  for (( i = 1; i <= total_seconds; i++ )); do
    local num=$i
    local sec=$((num % 60))
    local num=$((num / 60))
    local min=$((num % 60))
    local num=$((num / 60))
    local hour=$((num % 24))
    local day=$((num / 24))

    echo -ne "\r                                \r"
    printf "${CYAN}Contando... %02d:%02d:%02d:%02d" ${day} ${hour} ${min} ${sec}
    sleep 1
  done

  echo -e "${RED}\n-- ACABOU SEU TEMPO --"
  echo -e "${RED}\n$(date +"%a %e %h %Y %H:%M:%S")${RESET}\n"
}

usage() {
  cat <<EOF
[ $(basename "$0") ] - Programe seu tempo

  1) - 2,5 minutos
  2) - 5 minutos
  3) - 10 minutos
  4) - 10 segundos (Modo teste)

EOF
}

echo -ne "${GREEN}"
usage

echo -n "Informe a Opção: " 
read -r opcao

case "$opcao" in
  1) timer 150  ;;
  2) timer 300  ;;
  3) timer 600  ;;
  4) timer 10   ;;
  *) echo -e "${RED}Opção inválida!${RESET}" ;;
esac
